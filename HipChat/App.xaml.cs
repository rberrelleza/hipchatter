﻿using HipChat.Libs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// Il modello di applicazione vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=391641

namespace HipChat
{
    /// <summary>
    /// Fornisci un comportamento specifico dell'applicazione in supplemento alla classe Application predefinita.
    /// </summary>
    public sealed partial class App : Application
    {
        public Account account;

        public static Frame RootFrame { get; private set; }
        public static ContinuationManager ContinuationManager { get; private set; }

        public bool onBackground = false;

        /// <summary>
        /// Inizializza l'oggetto Application singleton. Si tratta della prima riga del codice creato
        /// eseguita e, come tale, corrisponde all'equivalente logico di main() o WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            ContinuationManager = new ContinuationManager();

            this.account = new Account();
        }

        private void Current_VisibilityChanged(object sender, Windows.UI.Core.VisibilityChangedEventArgs e)
        {
            if (e.Visible == false)
            {
                onBackground = true;
                API.Current.CloseConnection();
            }
            else if (onBackground)
            {
                onBackground = false;
                API.Current.Reconnect();
            }
        }

        /// <summary>
        /// Richiamato quando l'applicazione viene avviata normalmente dall'utente.  All'avvio dell'applicazione
        /// verranno utilizzati altri punti di ingresso per aprire un file specifico, per visualizzare
        /// risultati di ricerche e così via.
        /// </summary>
        /// <param name="e">Dettagli sulla richiesta e il processo di avvio.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
            CreateRootFrame();

            if (e.PreviousExecutionState == ApplicationExecutionState.Terminated || e.PreviousExecutionState == ApplicationExecutionState.Running || e.PreviousExecutionState == ApplicationExecutionState.Suspended)
            {
                API.Current.Reconnect();
            }

            if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                // Restore the saved session state only when appropriate
                try
                {
                    await SuspensionManager.RestoreAsync();
                }
                catch (SuspensionManagerException)
                {
                    //Something went wrong restoring state.
                    //Assume there is no state and continue
                }
            }

            if (!string.IsNullOrWhiteSpace(e.Arguments))
            {
                var toastLaunch = Regex.Match(e.Arguments, @"^jid://(?<arguments>.*)$");
                var toastActivationArgs = toastLaunch.Groups["arguments"];
                if (toastActivationArgs.Success)
                {
                    // The app has been activated through a toast notification click.
                    var jid = toastActivationArgs.Value;
                    RootFrame.Navigate(typeof(PublicRoomPage), jid);
                }
            }

            if (RootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter

                if (Account.Current.HasOfflinePresence)
                {
                    RootFrame.Navigate(typeof(OfflinePage), e.Arguments);
                }
                else
                {
                    if (Account.Current.HasSeenTutorial)
                    {
                        RootFrame.Navigate(typeof(Login), e.Arguments);
                    }
                    else
                    {
                        RootFrame.Navigate(typeof(TutorialPage), e.Arguments);
                    }
                }
            }

            // Assicurarsi che la finestra corrente sia attiva
            Window.Current.Activate();

            Window.Current.VisibilityChanged += Current_VisibilityChanged;
        }

        protected override void OnActivated(IActivatedEventArgs e)
        {
            //Check if this is a continuation
            var continuationEventArgs = e as IContinuationActivatedEventArgs;
            if (continuationEventArgs != null)
            {
                ContinuationManager.Continue(continuationEventArgs);
            }

            Window.Current.Activate();
        }

        private void CreateRootFrame()
        {
            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (RootFrame != null)
                return;

            // Create a Frame to act as the navigation context and navigate to the first page
            RootFrame = new Frame();

            // TODO: modificare questo valore su una dimensione di cache appropriata per l'applicazione
            RootFrame.CacheSize = 4;

            RootFrame.Background = new SolidColorBrush(Colors.Black);
            RootFrame.Foreground = new SolidColorBrush(Colors.White);

            //Associate the frame with a SuspensionManager key                                
            SuspensionManager.RegisterFrame(RootFrame, "AppFrame");

            // Set the default language
            RootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

            RootFrame.NavigationFailed += OnNavigationFailed;

            // Place the frame in the current Window
            Window.Current.Content = RootFrame;
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails.
        /// </summary>
        /// <param name="sender">The Frame which failed navigation.</param>
        /// <param name="e">Details about the navigation failure.</param>
        private void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Richiamato quando l'esecuzione dell'applicazione viene sospesa.  Lo stato dell'applicazione viene salvato
        /// senza che sia noto se l'applicazione verrà terminata o ripresa con il contenuto
        /// della memoria ancora integro.
        /// </summary>
        /// <param name="sender">Origine della richiesta di sospensione.</param>
        /// <param name="e">Dettagli relativi alla richiesta di sospensione.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            Account.Current.SaveOpenChats();
            API.Current.CloseConnection();

            await SuspensionManager.SaveAsync();
            ContinuationManager.MarkAsStale();

            // TODO: Salvare lo stato dell'applicazione e interrompere qualsiasi attività in background
            deferral.Complete();
        }

        public async void OpenHipChatXmppPage()
        {
            await Launcher.LaunchUriAsync(new System.Uri("http://hipchat.com/account/xmpp"));
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame == null)
            {
                return;
            }

            if (frame.CanGoBack)
            {
                frame.GoBack();
                e.Handled = true;
            }
        }
    }
}