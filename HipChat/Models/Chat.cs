﻿using HipChat.Libs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipChat.Model
{
    public class Chat
    {
        public object Resource;
        public Matrix.Jid Jid { get; set; }
        public string Name;

        private bool _isTyping = false;

        public ObservableCollection<ChatMessage> Messages = new ObservableCollection<ChatMessage>();

        /// <summary>
        /// The Resource name. Could be the room name, or the contact name. 
        /// </summary>
        public string ResourceName
        {
            get
            {
                if (IsRoom) return (Resource as Room).Name;
                return (Resource as Contact).Name;
            }
        }

        /// <summary>
        /// The Resource name. Could be the room name, or the contact name. 
        /// </summary>
        public string ResourceTypeDescription
        {
            get
            {
                if (IsRoom) return ((Resource as Room).IsPrivate ? "(Private) " : "") + (Resource as Room).DescriptionActivity;
                return "Private chat with user";
            }
        }

        /// <summary>
        /// The online state of the chatroom.
        /// When a room is public returns PublicRoom, while returns the user online state when a room is 1 to 1 (private).
        /// </summary>
        public OnlineState OnlineState {
            get
            {
                if (IsRoom) return OnlineState.PublicRoom;

                return (Resource as Contact).ExtendedOnlineState;
            }
        }

        /// <summary>
        /// Return the string label to use when leaving the chat
        /// </summary>
        public string LeaveDescription
        {
            get
            {
                return IsRoom ? "Leave room" : "Close chat";
            }
        }

        /// <summary>
        /// Returns true when the chatroom is a public room
        /// </summary>
        public bool IsRoom
        {
            get
            {
                return Resource.GetType() == typeof(Room);
            }
        }

        /// <summary>
        /// Tells the server whether the chat room is currently active
        /// </summary>
        public bool Active
        {
            set {
                API.Current.SetChatActiveStatus(this, value);
            }
        }

        /// <summary>
        /// Tells the server whether the user is typing a message into the chatroom
        /// </summary>
        public bool IsTyping
        {
            set
            {
                if (value == _isTyping || IsRoom) return;
                _isTyping = value;
                API.Current.SetChatTypingStatus(this, value);
            }
        }

        /// <summary>
        /// Sends a message to the chatroom
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(string message)
        {
            if (IsRoom)
            {
                API.Current.Client.Send(new Matrix.Xmpp.Client.Message
                {
                    From = API.Current.CurrentUser.Jid,
                    To = Jid,
                    Type = Matrix.Xmpp.MessageType.groupchat,
                    Body = message
                });
                (Resource as Room).LastActive = DateTime.Now;
            }
            else
            {
                API.Current.Client.Send(new Matrix.Xmpp.Client.Message
                {
                    From = API.Current.CurrentUser.Jid,
                    To = Jid,
                    Type = Matrix.Xmpp.MessageType.chat,
                    Body = message
                });

                var msg = new ChatMessage
                {
                    From = API.Current.CurrentUser.Jid,
                    Type = Matrix.Xmpp.MessageType.chat,
                    Body = message,
                    Stamp = DateTime.Now
                };

                Messages.Add(msg);
            }

            IsTyping = false;
        }

        /// <summary>
        /// Leaves the chat
        /// </summary>
        public void Leave()
        {
            API.Current.LeaveChatRoom(this);
        }
    }
}
