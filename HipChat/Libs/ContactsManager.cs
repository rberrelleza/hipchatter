﻿using HipChat.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Phone.PersonalInformation;

namespace HipChat.Libs
{
    public class ContactsManager
    {
        public static async void AddContacts(IEnumerable<Contact> contacts)
        {
            System.Diagnostics.Debug.WriteLine("Adding contacts to the phone..");

            ContactStore store = await ContactStore.CreateOrOpenAsync();

            foreach (Contact contact in contacts)
            {
                SaveContact(store, contact);
            }
        }

        private static async void SaveContact(ContactStore store, Contact person)
        {
            StoredContact contact = await store.FindContactByRemoteIdAsync(person.Jid);

            if (contact == null)
            {
                contact = new StoredContact(store); contact.GivenName = person.Name;
                contact.RemoteId = person.Jid;

                System.Diagnostics.Debug.WriteLine("Adding " + contact.RemoteId + " to the phone..");

                IDictionary<string, object> props = await contact.GetPropertiesAsync();
                props.Add(KnownContactProperties.Nickname, person.MentionName);

                IDictionary<string, object> extprops = await contact.GetExtendedPropertiesAsync();
                extprops.Add("HipChat mention name", person.MentionName);

                await contact.SaveAsync();
            }
        }

        async public static void DeleteAllContacts()
        {
            ContactStore store = await ContactStore.CreateOrOpenAsync();
            ContactQueryResult result = store.CreateContactQuery();
            IReadOnlyList<StoredContact> contacts = await result.GetContactsAsync();

            foreach (StoredContact contact in contacts)
            {
                await store.DeleteContactAsync(contact.Id);
            }
        }
    }
}
