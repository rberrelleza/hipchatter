﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.PushNotifications;
using Windows.UI.Popups;

namespace HipChat.Libs
{
    class CloudSocket
    {
        private static string EntryPoint = "http://api.squallstar.it:2600";

        private static string ToPercentEncoding(List<KeyValuePair<string, string>> pairs)
        {
            List<string> joinedPairs = new List<string>();
            foreach (var pair in pairs)
            {
                joinedPairs.Add(
                    System.Net.WebUtility.UrlEncode(pair.Key) +
                    "=" +
                    System.Net.WebUtility.UrlEncode(pair.Value));
            }

            return String.Join("&", joinedPairs);
        }

        public static void Close()
        {
            Open(false);
        }

        public static async void Open(bool open = true)
        {
            if (!Account.Current.isLoggedIn || !Account.Current.HasPushNotificationsEnabled || Account.Current.HasOfflinePresence) return;

            PushNotificationChannel channel = null;

            try
            {
                channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("jid", Account.Current.Email),
                    new KeyValuePair<string, string>("password", Account.Current.Password),
                    new KeyValuePair<string, string>("status", Account.Current.OnlinePresence),
                    new KeyValuePair<string, string>("channel_url", channel.Uri)
                };

                var content = new StringContent(ToPercentEncoding(pairs));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                var client = new HttpClient();
                await client.PostAsync(EntryPoint + "/" + (open ? "open" : "close"), content);
            }

            catch (Exception)
            {
                // Could not create a channel. 
            }
        }

        public static async void SendFeedback(string message)
        {
            try
            {
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("jid", Account.Current.Email),
                    new KeyValuePair<string, string>("message", message)
                };

                var content = new StringContent(ToPercentEncoding(pairs));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                var client = new HttpClient();
                await client.PostAsync(EntryPoint + "/send-feedback", content);

                var dlg = new MessageDialog("Your feedback has been sent. Thanks again for your message.", "Thanks!");
                
                dlg.Commands.Add(new UICommand
                {
                    Label = "Ok",
                    Id = 0
                });

                await dlg.ShowAsync();
            }

            catch (Exception)
            {
                // Could not send the feedback 
            }
        }
    }
}
