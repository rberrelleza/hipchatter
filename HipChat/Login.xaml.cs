﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Documents;
using HipChat.Libs;
using Matrix.Xmpp.Sasl;
using Matrix.Xmpp.Client;
using Matrix;
using Windows.UI.Popups;
using Windows.UI.Core;
using HipChat.Model;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=391641

namespace HipChat
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        private XmppClient client;

        private bool isLogging = false;

        public Login()
        {
            this.InitializeComponent();
            //this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            client = API.Current.Client;
            client.OnLogin += Client_OnLogin;
            client.OnAuthError += Client_OnAuthError;
            client.OnError += Client_OnError;

            isLogging = false;

            if (Frame.CanGoBack)
            {
                Frame.BackStack.RemoveAt(0);
            }

            this.email.Text = Account.Current.Email ?? "";

            if (Account.Current.isLoggedIn)
            {
                System.Diagnostics.Debug.WriteLine("User is already logged in.");
                this.password.Password = Account.Current.Password;
                DoLogin();
            }
            else
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendView("login");

                this.password.Password = "";

                Loader.Visibility = Visibility.Collapsed;
                LoginMask.Visibility = Visibility.Visible;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            client.OnLogin -= Client_OnLogin;
            client.OnAuthError -= Client_OnAuthError;
            client.OnError -= Client_OnError;
        }

        private void DoLogin()
        {
            if (isLogging) return;
            isLogging = true;

            System.Diagnostics.Debug.WriteLine("Opening XMPP connection...");

            LoginMask.Visibility = Visibility.Collapsed;
            Loader.Visibility = Visibility.Visible;

            if (this.email.Text.IndexOf("@conf.hipchat.com") != -1)
            {
                client.Username = this.email.Text;
            }
            else
            {
                client.Username = this.email.Text.Replace("@chat.hipchat.com", "") + "@chat.hipchat.com";
            }
            
            client.Password = this.password.Password;

            API.Current.OpenConnection();
        }

        private void ResetUI()
        {
            LoginMask.Visibility = Visibility.Visible;
            Loader.Visibility = Visibility.Collapsed;
        }

        private void Client_OnLogin(object sender, Matrix.EventArgs e)
        {
            Account.Current.Login(client.Username, client.Password);
            client.OnRosterEnd += Client_OnRosterEnd;
        }

        private void Client_OnRosterEnd(object sender, Matrix.EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Roaster received.");

            client.OnRosterEnd -= Client_OnRosterEnd;

            // Save the contacts on the phone
            ContactsManager.AddContacts(API.Current.Contacts);

            Frame.Navigate(typeof(MainPage));
        }

        private async void Client_OnAuthError(object sender, SaslEventArgs e)
        {
            isLogging = false;

            var dlg = new MessageDialog("Please double check your login details. Make sure that you're using the correct username, rather than your email.\r\n\r\nIt usually has this format: 1234_5678\r\n\r\nMore info here:\r\nhipchat.com/account/xmpp", "Login details not valid");
            dlg.Commands.Add(new UICommand("Ok"));

            ResetUI();
            await dlg.ShowAsync();
        }

        private async void Client_OnError(object sender, Matrix.ExceptionEventArgs e)
        {
            isLogging = false;

            var dlg = new MessageDialog("We were unable to contact the server. Please try later or check your internet connection.", "Error");
            dlg.Commands.Add(new UICommand("Ok"));

            ResetUI();
            await dlg.ShowAsync();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (email.Text == "")
            {
                email.Focus(FocusState.Keyboard);
                return;
            }

            if (password.Password == "")
            {
                password.Focus(FocusState.Keyboard);
                return;
            }

            DoLogin();
        }

        private async void infoEmail_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var dlg = new MessageDialog("Your HipChat username has a format like 1234_5678.\r\n\r\nFind yours here:\r\nhipchat.com/account/xmpp", "Jabber ID");
            dlg.Commands.Add(new UICommand {
                Label = "Ok",
                Id = 0
            });
            dlg.Commands.Add(new UICommand {
                Label = "Open website",
                Id = 1
            });
            dlg.CancelCommandIndex = 0;

            IUICommand command = await dlg.ShowAsync();

            if (command.Id.ToString() == "1")
            {
                (Application.Current as App).OpenHipChatXmppPage();
            }
        }

        private void password_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                LoginButton_Click(null, null);
            }
        }

        private void email_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                password.Focus(FocusState.Keyboard);
            }
        }
    }
}
