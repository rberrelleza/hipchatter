﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HipChat.Model;
using HipChat.Libs;
using Matrix.Xmpp.Client;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HipChat
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        bool hasRooms = true;
        bool hasChats = true;

        public MainPage()
        {
            this.InitializeComponent();

            this.peopleList.ItemsSource = API.Current.Contacts;
            this.roomsList.ItemsSource = API.Current.Rooms;
            this.chatsList.ItemsSource = API.Current.Chats;
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            API.Current.Rooms.CollectionChanged += Rooms_CollectionChanged;
            API.Current.Chats.CollectionChanged += Chats_CollectionChanged;

            if (API.Current.Chats.Count == 0)
            {
                if (hasChats)
                {
                    // Hides the chat pivot item when there are no active chats
                    hasChats = false;
                    mainPivot.Items.Remove(chatsPivotItem);
                }
            }
            else if (!hasChats)
            {
                // Re-adds the chat pivot item
                mainPivot.Items.Add(chatsPivotItem);
            }

            if (API.Current.Rooms.Count == 0 && hasRooms)
            {
                mainPivot.Items.Remove(roomsPivotItem);
                hasRooms = false;
            }

            if (API.Current.Contacts.Count == 0)
            {
                loader.IsActive = true;
                API.Current.Contacts.CollectionChanged += Contacts_CollectionChanged;
            }

            while (this.Frame.CanGoBack)
            {
                System.Diagnostics.Debug.WriteLine("Clearing back state");
                this.Frame.BackStack.RemoveAt(0);
            }

            GoogleAnalytics.EasyTracker.GetTracker().SendView("main-view");

            if (!API.Current.IsConnected)
            {
                loader.IsActive = true;

                API.Current.Reconnect(delegate(bool success)
                {
                    if (loader.IsActive)
                    {
                        loader.IsActive = false;
                    }
                });
            }
        }

        private void Contacts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (loader.IsActive)
            {
                loader.IsActive = false;
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            API.Current.Rooms.CollectionChanged -= Rooms_CollectionChanged;
            API.Current.Contacts.CollectionChanged -= Contacts_CollectionChanged;
            API.Current.Chats.CollectionChanged -= Chats_CollectionChanged;
        }

        private void Rooms_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (!hasRooms)
            {
                hasRooms = true;
                mainPivot.Items.Add(roomsPivotItem);
            }
        }

        private void Chats_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (!hasChats)
            {
                hasChats = true;
                mainPivot.Items.Add(chatsPivotItem);

                if (Account.Current.ShouldJumpToRecents)
                {
                    mainPivot.SelectedItem = chatsPivotItem;
                }
            }
            else if (hasChats && API.Current.Chats.Count == 0)
            {
                // Hides the chat pivot item when there are no active chats
                hasChats = false;
                mainPivot.Items.Remove(chatsPivotItem);
            }
        }

        private void Room_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Room room = (sender as StackPanel).DataContext as Room;

            API.Current.JoinRoom(room);
            Frame.Navigate(typeof(PublicRoomPage), room.chat.Jid.Bare);
        }

        private void Person_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Contact person = (sender as Grid).DataContext as Contact;

            API.Current.JoinOneToOneRoom(person);
            Frame.Navigate(typeof(PublicRoomPage), person.chat.Jid.Bare);
        }

        private void Chat_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Chat chat = (sender as StackPanel).DataContext as Chat;
            Frame.Navigate(typeof(PublicRoomPage), chat.Jid.Bare);
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            Account.Current.Logout();
            API.Current.Logout();

            Frame.Navigate(typeof(Login));
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private void Leave_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Chat chat = (sender as TextBlock).DataContext as Chat;
            chat.Leave();
        }

        private void StatusSelect_Click(object sender, RoutedEventArgs e)
        {
            MenuFlyoutItem selectedItem = sender as MenuFlyoutItem;

            if (selectedItem != null)
            {
                API.Current.SetPresence(selectedItem.Tag.ToString());
            }
        }

        private void GoOffline_Click(object sender, RoutedEventArgs e)
        {
            API.Current.SetOfflinePresence();
            Frame.Navigate(typeof(OfflinePage));
        }
    }
}
